var http = require('http'),
    parser = require('body-parser'),
    request = require('request'),
    port = process.env.PORT || 8080,
    teleToken = process.env.TELE_TOKEN,
    teleUrl = "https://api.telegram.org/bot",
    takeadas = require('./takeadas'),
    postParser = parser.json();

function command(cmd, params, message) {
  switch (cmd) {
    case 'duck':
      cmdDuck(params, message);
      break;
    case 'xnxx':
      cmdXnxx(params, message);
      break;
    case 'takear':
      takear(params, message);
      break;
  }
}

function cmdDuck(params, message) {
  var base = "https://duckduckgo.com/?q={{search}}";
  siteSearch(params, message, base);
}

function cmdXnxx(params, message) {
  var base = "http://www.xnxx.com/?k={{search}}";
  siteSearch(params, message, base);
}

function siteSearch(params, message, base) {
  var chat_id  = message.chat.id;
  var msg = base.replace("{{search}}", params.join('+'));
  var url = teleUrl + teleToken;
  request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: msg}});
}

function takear(params, message) {
  var chat_id  = message.chat.id;
  var rand = Math.floor(Math.random() * takeadas.length);
  var msg = '/me golpea a ' + params.join(' ') + ' con un ' + takeadas[rand];
  var url = teleUrl + teleToken;
  request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: msg}});
}

function handleRequest(req, res) {
  if (req.method == 'POST') {
    postParser(req, res, function () {
      if (req.url == '/'
          && typeof req.body.message != 'undefined'
          && typeof req.body.message.text != 'undefined'
          && req.body.message.text.substr(0, 1) == '/') {
        console.log(req.body);
        var params = req.body.message.text.split(' ');
        var cmd = params.shift().substr(1);
        if (cmd != "") {
          command(cmd, params, req.body.message);
        }
      }
      if (req.url.indexOf('/githook/') == 0 && ['push', 'tag_push'].indexOf(req.body.object_kind) >= 0) {
        // hardcore!!! patu patu patu! LiveTV group == -107333541
        var url = teleUrl + teleToken,
          chat_id =  -107333541,
          text = `User: ${req.body.user_email} ${req.body.user_avatar}
Project: ${req.body.project.name} ${req.body.project.web_url}
`;
        if (typeof req.body.commits != undefined && req.body.commits.length) {
          req.body.commits.map(function (commit) {
            text += `${commit.message.trim()}:
${commit.url}
`;
          })

        }
        request.post(url + "/sendMessage", {form: {chat_id: chat_id, text: text}});
      }
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end('okay');
    });
    return;
  }
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('okay');
}

var server = http.createServer(handleRequest);
server.listen(port, function () {
  console.log("Server listening on: http://localhost:%s", port);
});
